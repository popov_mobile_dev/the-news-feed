//
//  DateExtension.swift
//  The news feed
//
//  Created by Artyom on 27.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import Foundation


extension Date {
    
    func parseToString() -> String {
        let formatterStr = DateFormatter()
        let tempLocale = formatterStr.locale
        formatterStr.locale = tempLocale
        formatterStr.dateFormat = "dd.MM.yyyy"
        return formatterStr.string(from: self)
    }
    
    func parseTimeStampInSeconds(_ seconds: Double) -> Int {
        let nowDate = Date().timeIntervalSince1970
        let result = (Int(seconds)) - Int(nowDate)
        return result
    }
}
