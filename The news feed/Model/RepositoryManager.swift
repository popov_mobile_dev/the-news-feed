//
//  RepositoryManager.swift
//  The news feed
//
//  Created by Artyom on 21.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import ObjectMapper

enum UserDefaultKey: String {
    case currentPage
}

class RepositoryManager {
    
    var container: NSPersistentContainer? =
        (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    
    func createFetchResultsControllerForNewsFeedSection() -> NSFetchedResultsController<Article>? {
        guard let context = container?.viewContext else { return nil }
        
        let request: NSFetchRequest<Article> = Article.fetchRequest() as! NSFetchRequest<Article>
        request.sortDescriptors = [ NSSortDescriptor(key: "publishedAt", ascending: false) ]
        
        return NSFetchedResultsController<Article>(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil)
    }

    func cleareDataBase() {
        guard let context = container?.viewContext else { return }
        let request: NSFetchRequest<Article> = Article.fetchRequest() as! NSFetchRequest<Article>
        if let results = try? context.fetch(request) {
            for item in results {
                context.delete(item)
            }
            try? context.save()
        }
        
        savePaginationPage(number: 2)
    }
    
    func saveOrUpdateListOfNews(with data: Data) {
        guard let mainJson = try? JSONSerialization.jsonObject(with: data) as? [String: Any],
            let articlesJson = mainJson["articles"] as? [[String: Any]] else {
                print("Parse error")
                return
        }
        print("main: \(mainJson)")
        saveOrUpdateEntitiesToDataBase(with: articlesJson)
    }
    
    private func saveOrUpdateEntitiesToDataBase(with articlesJson: [[String : Any]]) {
        container?.performBackgroundTask { [weak self] context in
            
            var listOfNews: [Article] = []
            
            articlesJson.forEach { json in
                if let dateStr = json["publishedAt"] as? String,
                    let date = dateStr.parseToDate() {
                    
                    let request: NSFetchRequest<Article> = Article.fetchRequest() as! NSFetchRequest<Article>
                    request.predicate = NSPredicate(format: "publishedAt == %@", date as CVarArg)
                    
                    let results = try? context.fetch(request)
                    
                    if results?.count == 0 || results == nil {
                        let article = Article(context: context)
                        let map = Map(mappingType: .fromJSON, JSON: json)
                        article.mapping(map: map)
                        listOfNews.append(article)
                    }
                }
            }
            
            print("TOTAL ARTICLES \(listOfNews.count)")
            
            try? context.save()
            self?.checkStatistic()
        }
    }
    
    private func checkStatistic() {
        guard let context = container?.viewContext else { return }
        context.perform {
            if let articles = try? context.fetch(Article.fetchRequest()) {
                print("count articles in data base: \(articles.count)")
            }
        }
    }
}


//MARK: - analize request
extension RepositoryManager {
    
    func getLastPage() -> Int {
         let page = UserDefaults.standard.value(forKeyPath: UserDefaultKey.currentPage.rawValue) as? Int
         if let page = page {
             return page
         } else {
             return 2
         }
     }
    
    func analizeOfRequest(
        response data: Data?,
        typeOf request: RequestType,
        with typeOfUpdate: TypeOfUpdate) {
        
        switch typeOfUpdate {
        case .last:
            return
        case .previousPage:
            let currentPage = UserDefaults.standard.value(forKeyPath: UserDefaultKey.currentPage.rawValue) as? Int
            if let currentPage = currentPage {
                savePaginationPage(number: currentPage + 1)
            } else {
                savePaginationPage(number: 2)
            }
        }
    }
    
    fileprivate func savePaginationPage(number page: Int) {
        UserDefaults.standard.setValue(page, forKey: UserDefaultKey.currentPage.rawValue)
    }
}
