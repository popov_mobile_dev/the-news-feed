//
//  File.swift
//  The news feed
//
//  Created by Artyom on 21.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class Article: NSManagedObject, Mappable {
    
    @NSManaged var author: String?
    @NSManaged var nameOfPublication: String?
    @NSManaged var title: String?
    @NSManaged var summary: String?
    @NSManaged var imgURL: String?
    @NSManaged var articleURL: String?
    @NSManaged var publishedAt: Date?

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        author <- map["author"]
        nameOfPublication <- map["source.name"]
        title <- map["title"]
        summary <- map["description"]
        imgURL <- map["urlToImage"]
        articleURL <- map["url"]
        
        if let dateString = map["publishedAt"].currentValue as? String {
           publishedAt = dateString.parseToDate()
        }
    }
}

