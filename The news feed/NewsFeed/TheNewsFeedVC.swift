//
//  ViewController.swift
//  The news feed
//
//  Created by Artyom on 20.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import UIKit
import CoreData


class TheNewsFeedVC: UIViewController {
    
    fileprivate var presenter: TheNewsFeedPresenterProtocol!
    
    fileprivate var tableView = UITableView()
    fileprivate var topRefreshView = UIRefreshControl()
    
    fileprivate var bottomRefreshView = UIActivityIndicatorView()
    fileprivate var bottomRefreshHeightConstraint: NSLayoutConstraint?
    
    fileprivate var idCell: String {
        get {
            return "artileCell"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = TheNewsFeedPresenter(view: self)
        presenter.updateArticles(with: .last)
        configureViews()
    }
    
    private func configureViews() {
        configurateTableView()
        configurateBottomRefreshView()
    }
    
    private func configurateTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.allowsSelection = false
        tableView.register(TheNewsFeedTableViewCell.self, forCellReuseIdentifier: idCell)
        tableView.refreshControl = topRefreshView
        ///
        ///
        topRefreshView.addTarget(self, action: #selector(refreshControlActivate), for: .valueChanged)
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: UIApplication.shared.statusBarFrame.height),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])
    }
    
    private func configurateBottomRefreshView() {
        view.addSubview(bottomRefreshView)
        bottomRefreshView.translatesAutoresizingMaskIntoConstraints = false
        bottomRefreshView.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        bottomRefreshView.color = .white
        bottomRefreshHeightConstraint = bottomRefreshView.heightAnchor.constraint(equalToConstant: 0)
        bottomRefreshHeightConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            bottomRefreshView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomRefreshView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bottomRefreshView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    @objc func refreshControlActivate() {
        presenter.updateArticles(with: .last)
    }
}


//MARK: - tableView and scrollView delegate
extension TheNewsFeedVC: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollYDownPosition = scrollView.contentOffset.y + scrollView.frame.height
        if scrollView.contentSize.height <= scrollYDownPosition && !bottomRefreshView.isAnimating {
            activateBottomRefreshControl()
            presenter.updateArticles(with: .previousPage)
        }
    }
}


//MARK: - table view data source
extension TheNewsFeedVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getCellCount(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let article = presenter.getArticle(at: indexPath) else {
            return UITableViewCell()
        }
        let cell =
            tableView.dequeueReusableCell(withIdentifier: idCell, for: indexPath) as! TheNewsFeedTableViewCell
        cell.configureCell(for: article)
        return cell
    }
}


//MARK: - view model callback
extension TheNewsFeedVC: TheNewsFeedViewProtocol {
    func finishUpdate(isSucess: Bool, typeOfUpdate: TypeOfUpdate) {
        switch typeOfUpdate {
        case .last:
             tableView.refreshControl?.endRefreshing()
        case .previousPage:
            deactivateBottomRefreshControl()
        }
    }
}


//MARK: - fetch result constroller
extension TheNewsFeedVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        default:
            return
        }
    }
    
    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?) {
    
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        default:
            assertionFailure("Неизвестный статус")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}


//MARK: - bottom refresh controller
extension TheNewsFeedVC {
    
    func activateBottomRefreshControl() {
        bottomRefreshView.startAnimating()
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 0.8,
            initialSpringVelocity: 0,
            animations: {
                self.bottomRefreshHeightConstraint?.constant = 30
                self.view.layoutIfNeeded()
            })
    }
    
    func deactivateBottomRefreshControl() {
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.bottomRefreshHeightConstraint?.constant = 0
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.bottomRefreshView.stopAnimating()
            })
    }
}
