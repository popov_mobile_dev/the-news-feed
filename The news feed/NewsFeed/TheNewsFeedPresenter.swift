//
//  ArticlesViewModel.swift
//  The news feed
//
//  Created by Artyom on 24.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import Foundation
import CoreData


protocol TheNewsFeedViewProtocol: AnyObject {
    func finishUpdate(isSucess: Bool, typeOfUpdate: TypeOfUpdate)
}

protocol TheNewsFeedPresenterProtocol {
    func getCellCount(in section: Int) -> Int
    func getArticle(at indexPath: IndexPath) -> Article?
    func updateArticles(with typeOfUpdate: TypeOfUpdate)
}


class TheNewsFeedPresenter: TheNewsFeedPresenterProtocol {
    
    private weak var view: TheNewsFeedViewProtocol?
    
    private let repository: RepositoryManager
    private let requestManager: RequestManager
    private var fetchResultController: NSFetchedResultsController<Article>?
    
    init(view: TheNewsFeedViewProtocol) {
        self.view = view
        
        repository = RepositoryManager()
        requestManager = RequestManager(repository)
        
        repository.cleareDataBase()
        configFetchResultController()
    }
        
    func updateArticles(with typeOfUpdate: TypeOfUpdate) {
        requestManager.makeRequest(
            typeOf: .breakingNews,
            with: typeOfUpdate) { [weak self] isSuccess in
                self?.view?.finishUpdate(
                    isSucess: isSuccess,
                    typeOfUpdate: typeOfUpdate)
        }
    }
    
    
    //MARK: - data
    func getCellCount(in section: Int) -> Int {
        guard let sections = fetchResultController?.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func getArticle(at indexPath: IndexPath) -> Article? {
        return fetchResultController?.object(at: indexPath)
    }
    
    private func configFetchResultController() {
        fetchResultController = repository.createFetchResultsControllerForNewsFeedSection()
        try? fetchResultController?.performFetch()
        fetchResultController?.delegate = view as? NSFetchedResultsControllerDelegate
    }
}
