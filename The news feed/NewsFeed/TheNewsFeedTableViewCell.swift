//
//  ArticleTableViewCell.swift
//  The news feed
//
//  Created by Artyom on 24.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import UIKit
import Kingfisher


class TheNewsFeedTableViewCell: UITableViewCell {
    
    fileprivate var timeLabel = UILabel()
    fileprivate var titleLabel = UILabel()
    fileprivate var articleImgView = UIImageView()
    fileprivate var descriptionLabel = UILabel()
    
    fileprivate var refreshView = UIActivityIndicatorView()
    
    private var currentURLImage: URL?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configConstraint()
        configStyle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        articleImgView.image = nil
        articleImgView.backgroundColor = .gray
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func configureCell(for article: Article) {
        timeLabel.text = article.publishedAt?.parseToString() ?? "" 
        titleLabel.text = article.title
        descriptionLabel.text = article.summary
        
        if let imgAddress = article.imgURL,
           let url = URL(string: imgAddress) {
            
            currentURLImage = url
            refreshView.startAnimating()
            
            KingfisherManager
                .shared
                .retrieveImage(
                    with: url,
                    options: [],
                    progressBlock: nil,
                    downloadTaskUpdated: nil,
                    completionHandler: { [weak self] result in
                        guard let self = self else { return }
                        
                        self.refreshView.stopAnimating()
                        
                        switch result {
                        case .success(let val):
                            self.articleImgView.backgroundColor = .clear
                            if self.currentURLImage == url {
                                self.articleImgView.image = val.image
                            } else {
                                print("Разные адреса!")
                            }
                            
                        case .failure(let err):
                            self.articleImgView.image = UIImage(named: "image_not_found")
                            self.articleImgView.backgroundColor = .clear
                            print("KingFisherError: \(err)")
                        }
                    })
        } else {
            articleImgView.image = UIImage(named: "image_not_found")
            self.articleImgView.backgroundColor = .clear
        }
    }
    
    private func configStyle() {
        titleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        
        timeLabel.font = UIFont(name: "Avenir-Book", size: 16)
        timeLabel.textColor = .gray
        descriptionLabel.numberOfLines = 1
        
        descriptionLabel.font = UIFont(name: "Avenir-Book", size: 16)
        descriptionLabel.textColor = .gray
        descriptionLabel.numberOfLines = 0
        
        articleImgView.backgroundColor = .clear
        articleImgView.contentMode = .scaleAspectFit
        articleImgView.clipsToBounds = true
        articleImgView.backgroundColor = .gray
        
        refreshView.style = .white
    }
    
    private func configConstraint() {
        let guide = contentView.layoutMarginsGuide
        
        contentView.addSubview(timeLabel)
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            timeLabel.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant:0),
            timeLabel.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            timeLabel.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0),
        ])
        
        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant:0),
            titleLabel.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            titleLabel.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 8)
        ])
        
        contentView.addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionLabel.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant:0),
            descriptionLabel.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            descriptionLabel.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: -8)
        ])
        
        contentView.addSubview(articleImgView)
        articleImgView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            articleImgView.heightAnchor.constraint(equalToConstant: 220),
            articleImgView.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0),
            articleImgView.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            articleImgView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            articleImgView.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -8)
        ])
        
        articleImgView.addSubview(refreshView)
        refreshView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            refreshView.centerYAnchor.constraint(equalTo: articleImgView.centerYAnchor),
            refreshView.centerXAnchor.constraint(equalTo: articleImgView.centerXAnchor)
        ])
    }
}
