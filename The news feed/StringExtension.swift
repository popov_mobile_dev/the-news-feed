//
//  StringExtension.swift
//  The news feed
//
//  Created by Artyom on 27.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import Foundation

extension String {
    func parseToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormatter.date(from: self)
    }
}
