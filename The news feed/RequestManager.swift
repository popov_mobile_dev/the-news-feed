//
//  File.swift
//  The news feed
//
//  Created by Artyom on 21.05.2020.
//  Copyright © 2020 yolo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum RequestType {
    case breakingNews
}

enum TypeOfUpdate {
    case last
    case previousPage
}


class RequestManager {
    
    init(_ repository: RepositoryManager) {
        self.repository = repository
    }
    
    private weak var repository: RepositoryManager?
    
    private let baseURL = "https://newsapi.org/v2/top-headlines"
    private let apiKey = "cbb33fe643524b019af1f1f1920f0bf8"
    
    
    func makeRequest(typeOf request: RequestType,
                     with typeOfUpdate: TypeOfUpdate,
                     complitionHandler: ((Bool) -> ())? = nil ) {
        
            let url = buildRequest(typeOf: request, with: typeOfUpdate)
            let header = buildHeader(typeOf: request)
            let httpMethod = prepareHTTPMethod(typeOf: request)
            
            AF.request(url, method: httpMethod, headers: header)
                .response(completionHandler: { [weak self] in
                    guard let rawResponse = $0.response else {
                        complitionHandler?(false)
                        return
                    }
                    
                    switch rawResponse.statusCode {
                    case 200...300:
                        self?.parseAnswer(
                            response: $0.data,
                            typeOf: request,
                            with: typeOfUpdate)
                        complitionHandler?(true)
                    case 401:
                        complitionHandler?(false)
                        print("Unauthorized error")
                    default:
                        complitionHandler?(false)
                        print("Request error")
                    }
                })
    }
    
    
    //MARK: - prepare request
    private func buildRequest(typeOf request: RequestType,
                              with typeOfUpdate: TypeOfUpdate) -> String {
        
        let countryParam = "country=ru"
        let pageSize = "pageSize=10"
        var page = "page=1"
        
        switch typeOfUpdate {
        case .last:
            break
        case .previousPage:
            page = "page=" + (repository?.getLastPage() ?? 2).description
        }
        
        switch request {
        case .breakingNews:
            return baseURL + "?" + countryParam + "&" + pageSize + "&" + page
        }
    }
    
    private func buildHeader(typeOf request: RequestType) -> HTTPHeaders {
        switch request {
        case .breakingNews:
            return [.init(name: "X-Api-Key", value: apiKey)]
        }
    }
    
    private func prepareHTTPMethod(typeOf request: RequestType) -> HTTPMethod {
        switch request {
        case .breakingNews:
            return .get
        }
    }
    
    //MARK: - answer
    private func parseAnswer(response data: Data?,
                             typeOf request: RequestType,
                             with typeOfUpdate: TypeOfUpdate) {
        
        guard let data = data else { return }
        
        switch request {
        case .breakingNews:
            repository?.analizeOfRequest(response: data,
                                         typeOf: request,
                                         with: typeOfUpdate)
            repository?.saveOrUpdateListOfNews(with: data)
        }
    }
}
